﻿# Get all boot options
$bootOptions = @(bcdedit /enum firmware | Select-String -Pattern "Firmware Application" -Context 0,6)

# Count how many boot options found
$bootCount = $bootOptions.Count

# Loop until all boot options are processed
for ($i=0; $i -lt $bootCount; $i++)
{
    # Find only the ones that are labeled Windows Boot Manager so we don't remove unnecessary ones
    if ($bootOptions[$i] -match "Windows Boot Manager"){
        # Get the identifier in the ones that are found to be true
        $identifier = ([regex]::match($bootOptions[$i],'{.*}'))
        # Display and delete the entry
        Write-Host $identifier
        bcdedit.exe /delete $identifier
    }
}